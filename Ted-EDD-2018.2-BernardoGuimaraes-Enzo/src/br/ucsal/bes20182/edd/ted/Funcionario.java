package br.ucsal.bes20182.edd.ted;

public class Funcionario {
	public Integer cpf;
	public String nome;
	public String profissao;
	public Funcionario(Integer cpf,String nome,String profissao) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.profissao = profissao;
	}



	public Integer getCpf() {
		return cpf;
	}

	public String getNome() {
		return nome;
	}

	public String getProfissao() {
		return profissao;
	}




}
